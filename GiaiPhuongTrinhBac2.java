package Solution;

import java.util.Scanner;

public class GiaiPhuongTrinhBac2 {
    public static void main(String[] args) {
        float a;
        float b;
        float c;
        float denta;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap he so a: ");
        a = sc.nextFloat();
        System.out.println("Nhap he so b: ");
        b = sc.nextFloat();
        System.out.println("Nhap he so c: ");
        c = sc.nextFloat();

        if (a == 0) {
            if(b==0 && c==0){
                System.out.println("Phuong Trinh co vo so nghiem");
            } else if (b==0 && c!=0) {
                System.out.println("Phuong trinh vo nghiem");
            } else {
                System.out.println("Phuong trinh co nghiem duy nhat: "+ -c/b);
            }

        } else {
            denta = b*b -4*a*c;
            if(denta<0){
                System.out.println("Phuong trinh vo nghiem");
            } else if(denta==0){
                float x = (-b)/(2*a);
                System.out.println("Phuong trinh co nghiem kep: "+"x = "+x );
            } else {
                float x1 = (-b-sqrt(denta))/(2*a);
                float x2 = (-b+sqrt(denta))/(2*a);
                System.out.println("Phuong trinh co 2 nghiem phan biet la:"+"x= "+x1 + " va "+"x= "+x2);
            }
        }
    }

    private static float sqrt(float denta) {
        return 0;
    }

}

